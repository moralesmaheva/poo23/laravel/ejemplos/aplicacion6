@extends('layouts.main')

@section('titulo', 'Listado de compras')

@section('cabecera')
    {{-- llamamos al componente cabecera --}}
    <x-cabecera subtitulo="Podemos ver todas las compras">
        <i class="fa-solid fa-shop"></i>Listado de compras
    </x-cabecera>
@endsection

@section('contenido')
    @if (session('mensaje'))
        <div class="row m-3">
            <div class="alert alert-info">
                {{ session('mensaje') }}
            </div>
        </div>
    @endif
    {{-- cargamos el componente listview --}}
    <x-listviewrelations :registros="$productotienda" modelo="productotienda" :campos="[
        'cantidad',
        'producto_id',
        'tienda_id',
        'nombretienda',
        'nombreproducto',
        'tienda.nombre',
        'producto.nombre',
        // [
        //     'label' => 'Nombre de la tienda',
        //     'campo' => 'nombre',
        //     'relacion' => 'tienda',
        // ],
        // [
        //     'label' => 'Nombre del producto',
        //     'valor' => function ($modelo) {
        //         return $modelo->tienda->nombre;
        //     },
        // ],
    ]" />
@endsection
