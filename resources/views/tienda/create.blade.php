@extends('layouts.main')

@section('titulo', 'Insertar')

@section('cabecera')
    <section class="pt-5 container">
        <div class="row py-lg-5">
            <h1 class="display-4 fw-bold lh-1 mb-3 text-light">Insertar una nueva tienda</h1>
            <p class="col-lg-10 fs-4 text-light">
                Desde este formulario puedes crear una nueva tienda
            </p>
        </div>
    </section>
@endsection

@section('contenido')

    {{-- cargo el componente formulario --}}
    <x-formularios.formulario :errors="$errors" method="POST"
        route="{{ route('tienda.store') }}">

        {{-- cargo el componente input para el nombre --}}
        <x-formularios.input label="Nombre" name="nombre" type="text" value="{{ old('nombre') }}" />

        {{-- cargo el componente input para la ubicación --}}
        <x-formularios.input label="Ubicación" name="ubicacion" type="text"
            value="{{ old('ubicacion') }}" />

        {{-- cierro el componente formulario --}}
    </x-formularios.formulario>
@endsection
