@extends('layouts.main')

@section('titulo', 'Mostrar')

@section('cabecera')
    <section class="pt-5 container">
        <div class="row py-lg-5">
            <h1 class="display-4 fw-bold lh-1 mb-3 text-white">Ver tienda</h1>
            <p class="col-lg-10 fs-4 text-white">
                Estos son los datos de la tienda solicitada
            </p>
        </div>
    </section>
@endsection

@section('contenido')
    @if (session('mensaje'))
        <div class="row m-2">
            <div class="alert alert-info">
                {{ session('mensaje') }}
            </div>
        </div>
    @endif
    <div class="row mt-3">
        <div class="col">
            <div class="card shadow-xl">
                <div class="card-body">
                    <h5 class="card-title">
                        {{ $tienda->id }}
                    </h5>
                    <p class="card-text">
                        Nombre de la tienda: {{ $tienda->nombre }}
                    </p>
                    <p class="card-text">
                        Ubicación de la tienda: {{ $tienda->ubicacion }}
                    <div class="card-footer">
                        <div class="vertical-align-bottom d-flex justify-content-between align-items-center">
                            <a href="{{ route('tienda.edit', $tienda->id) }}" class="btn btn-outline-primary">Editar</a>
                            <form action="{{ route('tienda.destroy', $tienda) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-outline-danger">Eliminar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection