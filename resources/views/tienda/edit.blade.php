@extends('layouts.main')

@section('titulo', 'Insertar')

@section('cabecera')
    <section class="pt-5 container">
        <div class="row py-lg-5">
            <h1 class="display-4 fw-bold lh-1 mb-3 text-white">Actualizar la tienda solicitada</h1>
            <p class="col-lg-10 fs-4">
                Desde este formulario puedes actualizar la tienda
            </p>
        </div>
    </section>
@endsection

@section('contenido')
    {{-- cargo el componente edit --}}
    {{-- <x-edit :registro="$tienda" modelo="tienda" :campos="['id', 'nombre', 'ubicacion']"  /> --}}

    {{-- cargo el componente formulario --}}
    <x-formularios.formulario :modelo="$tienda" :errors="$errors" method="PUT"
        route="{{ route('tienda.update', $tienda) }}">

        {{-- cargo el componente input para el nombre --}}
        <x-formularios.input label="Nombre" name="nombre" type="text" value="{{ old('nombre', $tienda->nombre) }}" />

        {{-- cargo el componente input para la ubicación --}}
        <x-formularios.input label="Ubicación" name="ubicacion" type="text"
            value="{{ old('ubicacion', $tienda->ubicacion) }}" />

    {{-- cierro el componente formulario --}}
    </x-formularios.formulario>
@endsection
