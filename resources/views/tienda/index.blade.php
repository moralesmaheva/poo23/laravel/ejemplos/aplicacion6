@extends('layouts.main')

@section('titulo', 'Listado de tiendas')

@section('cabecera')
{{-- llamamos al componente cabecera--}}
    <x-cabecera subtitulo="Podemos ver todas las tiendas">
        <i class="fa-solid fa-shop"></i>Listado de tiendas
    </x-cabecera>
@endsection

@section('contenido')
    @if (session('mensaje'))
        <div class="row m-3">
            <div class="alert alert-info">
                {{ session('mensaje') }}
            </div>
        </div>
    @endif
    {{-- cargamos el componente listview --}}
    <x-listview :registros="$tiendas" modelo="tienda" :campos="['id', 'nombre', 'ubicacion']" />

    @endsection