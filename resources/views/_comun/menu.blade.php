<nav class="navbar navbar-expand-lg bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ route('home.index') }}">Menu</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown"
            aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link active{{ request()->routeIs('home.index') ? ' active' : '' }}" aria-current="page" href="{{ route('home.index') }}">Inicio</a>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="{{ route('tienda.index') }}" role="button" data-bs-toggle="dropdown"
                        aria-expanded="false">
                        Tiendas
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="{{ route('tienda.index') }}">Listado de tiendas</a></li>
                        <li><a class="dropdown-item" href="{{ route('tienda.create') }}">Insertar tienda</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                        aria-expanded="false">
                        Productos
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="{{ route('producto.index') }}">Listado de productos</a></li>
                        <li><a class="dropdown-item" href="{{ route('producto.create') }}">Insertar producto</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown"
                        aria-expanded="false">
                        Compras
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="{{ route('productotienda.index') }}">Listado de compras</a></li>
                        <li><a class="dropdown-item" href="{{ route('productotienda.create') }}">Insertar compra</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
