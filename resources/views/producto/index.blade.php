@extends('layouts.main')

@section('titulo', 'Listado de productos')

@section('cabecera')
    <x-cabecera subtitulo="Podemos ver todos los productos">
        <i class="fa-solid fa-shop"></i>Listado de productos
    </x-cabecera>

@endsection

@section('contenido')
    @if (session('mensaje'))
        <div class="row m-3">
            <div class="alert alert-info">
                {{ session('mensaje') }}
            </div>
        </div>
    @endif
    {{-- cargamos el componente listview --}}
    <x-listview :registros="$productos" modelo="producto" :campos="['id', 'nombre', 'precio']" />
@endsection
