@extends('layouts.main')

@section('titulo', 'Insertar')

@section('cabecera')
    <section class="pt-5 container">
        <div class="row py-lg-5">
            <h1 class="display-4 fw-bold lh-1 mb-3">Actualizar el producto solicitado</h1>
            <p class="col-lg-10 fs-4">
                Desde este formulario puedes actualizar el producto
            </p>
        </div>
    </section>
@endsection

@section('contenido')
    {{-- cargo el componente formulario --}}
    <x-formularios.formulario :modelo="$producto" :errors="$errors" method="PUT"
        route="{{ route('producto.update', $producto) }}">

        {{-- cargo el componente input para el nombre --}}
        <x-formularios.input label="Nombre" name="nombre" type="text" value="{{ old('nombre', $producto->nombre) }}" />

        {{-- cargo el componente input para la ubicación --}}
        <x-formularios.input label="Precio" name="precio" type="number" value="{{ old('precio', $producto->precio) }}" />

        {{-- cierro el componente formulario --}}
    </x-formularios.formulario>
@endsection
