@extends('layouts.main')

@section('titulo', 'Inicio')

@section('cabecera')
    <section class="pt-5 text-center container">
        <div class="row py-lg-5">
            <div class="col-lg-6 col-md-8 mx-auto">
                <h1 class="fw-bold text-light">Aplicación Web</h1>
                <p class="lead text-light">Gestión de tiendas y productos</p>
            </div>
        </div>
    </section>
    @parent
@endsection

@section('contenido')
    <div class="row mt-3 text-center">
        <div>
            <img src="{{ asset('imgs/images.jpg') }}" class="img-fluid rounded col-8 mb-6" alt="Responsive image">
            <div class="d-grid gap-2 col-6 mx-auto mt-3">
                <a href="{{ route('tienda.index') }}" class="btn btn-light btn-lg">Abrir tiendas</a>
                <a href="{{ route('producto.index') }}" class="btn btn-light btn-lg">Abrir productos</a>
                <a href="{{ route('productotienda.index') }}" class="btn btn-light btn-lg">Abrir compras</a>
            </div>
        </div>
    </div>
@endsection