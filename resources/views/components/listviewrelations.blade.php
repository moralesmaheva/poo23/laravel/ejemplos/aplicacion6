<div class="row mt-1 row-cols-1 row-cols-lg-3 row-cols-md-2 g-4">
    @foreach ($registros as $registro)
        <div class="col">
            <div class="card shadow-sm text-bg-secondary border-2 border-light">
                <div class="card-body">
                    {{-- campos a mostrar --}}
                    @foreach ($campos as $campo)
                        <p class="card-text">
                            {{ $campo['label'] }}: {{ $campo['valor']($registro) }}
                        </p>
                    @endforeach
                </div>
                <div class="card-footer border-light">
                    <div class="vertical-align-bottom d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                            <a href="{{ route($modelo . '.show', $registro->id) }}" class="btn btn-outline-info">Ver</a>
                            <a href="{{ route($modelo . '.edit', $registro->id) }}"
                                class="btn btn-outline-info">Editar</a>
                        </div>
                        <form action="{{ route($modelo . '.destroy', $registro) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-outline-danger">Eliminar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
<div class="row mt-3">
    @if (method_exists($registros, 'links'))
        {{ $registros->links() }}
    @endif

</div>
