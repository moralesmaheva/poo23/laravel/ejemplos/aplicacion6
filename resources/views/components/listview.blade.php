@props(['registros', 'modelo' => '', 'comprobar' => false, 'campos' => []])


<div class="row mt-1 row-cols-1 row-cols-lg-3 row-cols-md-2 g-4">
    @foreach ($registros as $registro)
        <div class="col">
            <div class="card shadow-sm text-bg-secondary border-2 border-light">
                <div class="card-body">
                    {{-- compruebo si campos esta vacio y si no lo esta muestro solo los campos --}}
                    @if (!empty($campos))
                        @foreach ($campos as $value)
                            <p class="card-text">
                                {{ $registro->getAttributeLabel($value) }}: {{ $registro->$value }}
                            </p>
                        @endforeach
                    @else
                        @foreach ($registro->toArray() as $label => $value)
                            <p class="card-text">
                                {{ $label }}: {{ $value }}
                            </p>
                        @endforeach
                    @endif
                    {{-- campos de otras tablas a mostrar --}}
                    @if (!empty($ajenas))
                        @foreach ($ajenas as $index => $value)
                            @foreach ($value as $campo)
                                <p class="card-text">
                                    {{ $campo . ' ' . $index }}: {{ $registro->$index->$campo }}
                                </p>
                            @endforeach
                        @endforeach
                    @endif
                </div>
                <div class="card-footer border-light">
                    <div class="vertical-align-bottom d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                            <a href="{{ route($modelo . '.show', $registro->id) }}" class="btn btn-outline-info">Ver</a>
                            <a href="{{ route($modelo . '.edit', $registro->id) }}"
                                class="btn btn-outline-info">Editar</a>
                        </div>
                        <form action="{{ route($modelo . '.destroy', $registro) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-outline-danger">Eliminar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>
<div class="row mt-3">
    {{-- compruebo si esta paginado en el modelo --}}
    @if (method_exists($registros, 'links'))
        {{ $registros->links() }}
    @endif
</div>
