# Objetivos

Componente de clase que me permite mostrar en formato de tarjeta todos los registros de una consulta

# Uso

Para utilizarlo simplemente llamo al componente desde la vista y le mando el array de los modelos

# Parametros

- $registros : array de modelos
- $modelo : nombre del modelo que estamos utilizando
- $campos : campos a mostrar de la tabla. Si no le pasamos nada muestra todos los campos tanto los de la tabla como los personalizados en el modelo.
- $ajenas : me permite indicarle de las relaciones (claves ajenas) que campos quiero mostrar de la tabla relacionado (solo en relaciones a 1)


# Ejemplo

~~~php
    <x-listado :registros="$productotienda" modelo="productotienda" :campos="['cantidad', 'tienda_id', 'producto_id']" :ajenas="['tienda' => ['nombre', 'ubicacion'], 'producto' => ['nombre', 'precio']]" />
~~~

Hay que tener en cuenta que la variable productos sera una consulta realizada en el controlador

Sin paginar
~~~php
$productotienda = ProductoTienda::all();
~~~
Listo los productos paginados

~~~php
$productotienda = ProductoTienda::paginate(9);
~~~

Las claves ajenas tienen que ser un array asociativo en donde el indice es el nombre de la relacion creada en el modelo y despues un array con los campos de la otra tabla que quieres mostrar