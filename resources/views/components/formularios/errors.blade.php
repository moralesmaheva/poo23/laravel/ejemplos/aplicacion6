@props([
    'errors',
])

@if ($errors->any())
    <div class="row mt-3">
        <h2>Errores en el formulario</h2>
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif