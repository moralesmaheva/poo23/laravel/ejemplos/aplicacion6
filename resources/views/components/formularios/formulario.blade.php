
{{-- cargamos el componente de errors --}}
<x-formularios.errors :errors="$errors" />

<div class="row mt-3">
    <div class="col-lg-10 mt-2 mx-auto">
        {{-- pra que me envie el action y el metodo --}}
        <form action="{{ $route }}" method="post" class="p-4 p-md-5 border rounded-3 bg-light">
            @csrf
            @method($method)

            {{-- cargamos el componente a traves de un slot --}}
            {{ $slot }}

            <button type="submit" class="btn btn-outline-primary">{{ $boton }}</button>
            <button type="reset" class="btn btn-outline-primary">Limpiar</button>
    </div>
</div>