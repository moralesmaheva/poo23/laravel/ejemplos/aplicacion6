@props(['label', 'name', 'type' => 'text', 'value' => ''])

<div class="mb-3">
    <label for="{{ $name }}" class="form-label">{{ $label }}</label>
    <input type="{{ $type }}" class="form-control" id="{{ $name }}" name="{{ $name }}"
        placeholder="{{ $label }}" value="{{ $value }}">
</div>

{{-- cargando el componente de error --}}
<x-formularios.error name="{{ $name }}" />
