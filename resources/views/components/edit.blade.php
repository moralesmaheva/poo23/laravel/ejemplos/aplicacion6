@props([
    'registro',
    'campos' => [],
    'modelo' => '',
])


@if ($errors->any())
    <div class="row mt-3">
        <h2>Errores en el formulario</h2>
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
<div class="row mt-3">
    <div class="col-lg-10 mt-2 mx-auto">
        <form action="{{ route($modelo . '.update', $registro) }}" method="post" class="p-4 p-md-5 border rounded-3 bg-light">
            @csrf
            @method('PUT')
            @if (!empty($campos))
                @foreach ($registro->getAttributes() as $label => $value)
                    @if (in_array($label, $campos))
                        <div class="mb-3">
                            <label for="{{ $label }}" class="form-label"></label>
                            <input type="text" class="form-control" id="{{ $label }}"
                                name="{{ $label }} " value="{{ old($label, $value) }}">
                        </div>
                        <div>
                            @error('{{ $label }}')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                    @endif
                @endforeach
            @endif
            <button type="submit" class="btn btn-outline-primary">Actualizar</button>
    </div>
</div>
