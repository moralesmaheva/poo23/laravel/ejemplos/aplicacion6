@props([
    'izquierda' => 'Maheva Morales',
    'derecha' => 'Aplicación Web',	
])



<footer id="footer" class="bg-secondary footer mt-auto py-3 text-muted">
    <div class="container">
        <div class="row d-flex align-items-center" style="height: 50px">
            <div class="col-md-6 text-center text-md-start text-white">{{ $izquierda }}</div>
            <div class="col-md-6 text-center text-md-end text-white"> {{ $derecha }}</div>
        </div>
    </div>
</footer>
