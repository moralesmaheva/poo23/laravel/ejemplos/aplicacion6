<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\ProductoTiendaController;
use App\Http\Controllers\TiendaController;
use App\Models\Producto;
use Illuminate\Support\Facades\Route;

Route::controller(HomeController::class)->group(function () {
    Route::get('/', 'index')->name('home.index');
    Route::get('/index', 'index')->name('home.index');
});

//rutas de tienda con recurso
Route::resource('tienda', TiendaController::class);

//rutas para tienda
// Route::controller(TiendaController::class)->group(function () {
//     Route::get('/tienda', 'index')->name('tienda.index');
//     Route::get('/tienda/create', 'create')->name('tienda.create');
//     Route::get('/tienda/{id}', 'show')->name('tienda.show');
//     Route::post('/tienda', 'store')->name('tienda.store');
//     Route::get('/tienda/edit/{id}', 'edit')->name('tienda.edit');
//     Route::put('/tienda/{id}', 'update')->name('tienda.update');
//     Route::delete('/tienda/{id}', 'destroy')->name('tienda.destroy');
// });


//rutas de producto con recurso

Route::resource('producto', ProductoController::class);

// //rutas para producto
// Route::controller(ProductoController::class)->group(function () {
//     Route::get('/producto', 'index')->name('producto.index');
//     Route::get('/producto/create', 'create')->name('producto.create');
//     Route::get('/producto/{id}', 'show')->name('producto.show');
//     Route::post('/producto', 'store')->name('producto.store');
//     Route::get('/producto/edit/{id}', 'edit')->name('producto.edit');
//     Route::put('/producto/{id}', 'update')->name('producto.update');
//     Route::delete('/producto/{id}', 'destroy')->name('producto.destroy');
// });

//rutas para productoTienda
// Route::controller(ProductoTiendaController::class)->group(function () {
//     Route::get('/productoTienda', 'index')->name('productoTienda.index');
//     Route::get('/productoTienda/create', 'create')->name('productoTienda.create');
//     Route::get('/productoTienda/{id}', 'show')->name('productoTienda.show');
//     Route::post('/productoTienda', 'store')->name('productoTienda.store');
//     Route::get('/productoTienda/edit/{id}', 'edit')->name('productoTienda.edit');
//     Route::put('/productoTienda/{id}', 'update')->name('productoTienda.update');
//     Route::delete('/productoTienda/{id}', 'destroy')->name('productoTienda.destroy');
// });

//rutas para productotienda con resource
Route::resource('productotienda', ProductotiendaController::class);

