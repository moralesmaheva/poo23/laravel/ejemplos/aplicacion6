<?php

namespace App\Http\Controllers;

use App\Models\Productotienda;
use Illuminate\Http\Request;

class ProductotiendaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $productotienda = Productotienda::paginate(6);

        //dd($productotienda[3]->nombreproducto);

        return view('productotienda.index', ['productotienda' => $productotienda]);
    }



    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }



    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }



    /**
     * Display the specified resource.
     */
    public function show(Productotienda $productotienda)
    {

        $productotienda->tienda->nombre; //saco el nombre de la tienda
        $productotienda->producto->nombre; //saco el nombre del producto

        return view('productotienda.show', ['productotienda' => $productotienda]);
    }



    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Productotienda $productotienda)
    {
        //
    }



    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Productotienda $productotienda)
    {
        //
    }



    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Productotienda $productotienda)
    {
        //
    }
}
