<?php

namespace App\Http\Controllers;

use App\Http\Requests\TiendaRequest;
use App\Models\Tienda;
use Illuminate\Http\Request;

class TiendaController extends Controller
{
    public function index()
    {
        //paginacion de 5 en 5
        $tienda = Tienda::paginate(5);

        return view('tienda.index', ['tiendas' => $tienda]);
    }

    public function create()
    {
        return view('tienda.create');
    }


    public function store(TiendaRequest $request)
    {
        //crear producto
        $tienda = Tienda::create($request->all());

        //redireccionamos
        return redirect()
            ->route('tienda.show', $tienda->id)
            ->with('mensaje', 'La tienda se ha creado correctamente');
    }

    public function edit($id)
    {
        //recuperamos los datos del producto
        $tienda = Tienda::find($id);

        //mostrar el formulario de edicion
        return view('tienda.edit', ['tienda' => $tienda]);
    }


    public function update(TiendaRequest $request, Tienda $tienda)
    {
        //actualizar el producto
        $tienda->update($request->all());

        //redireccionamos
        return redirect()
            ->route('tienda.show', $tienda->id)
            ->with('mensaje', 'La tienda se ha actualizado correctamente');
    }

    public function show($id)
    {
        $tienda = Tienda::find($id);

        return view('tienda.show', ['tienda' => $tienda]);
    }


    public function destroy(Tienda $tienda)
    {
        //eliminamos poroducto
        $tienda->delete();

        //redireccionamos
        return redirect()
            ->route('tienda.index')
            ->with('mensaje', 'La tienda se ha eliminado correctamente');
    }
}
