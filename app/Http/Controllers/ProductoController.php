<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use Illuminate\Http\Request;

class ProductoController extends Controller
{
    public function index()
    {
       // listo los productos
        // $productos = Producto::all();
        // listo los productos paginados
        $productos = Producto::paginate(5);


        // $productos es un array(coleccion) de modelos
        // $productos[0] es un modelo (clase)
        // $productos[0]->nombre es un string con el nombre del producto
        // $productos[0]->getAttributes() es un array con los atributos de la tabla y valores
        // $productos[0]->toArray() es un array con los atributos de la tabla y valores
        // $productos->load('productotiendas') es para ejecutar las relaciones en todos los modelos




        // los mando a la vista
        return view(
            'producto.index',
            compact('productos')
        );

    }

    public function create()
    {
        return view('producto.create');
    }


    public function store(Request $request)
    {
        //para validar los datos
        $request->validate([
            'nombre' => 'required',
            'precio' => 'required|numeric|max:1000,00',
        ], [
            //mensajes de validación personalizada
            'required' => 'El :attribute es obligatorio',
            'precio.numeric' => 'El precio del producto debe ser numérico',
            'precio.max' => 'El precio del producto no debe ser mayor a :max',
        ], [
            'nombre' => 'Nombre',
            'precio' => 'Precio',
        ]);
        //crear producto
        $producto = Producto::create($request->all());

        //redireccionamos
        return redirect()
            ->route('producto.show', $producto->id)
            ->with('mensaje', 'El producto se ha creado correctamente');
    }

    public function edit($id)
    {
        //recuperamos los datos del producto
        $producto = Producto::find($id);

        //mostrar el formulario de edicion
        return view('producto.edit', ['producto' => $producto]);
    }


    public function update(Request $request, Producto $producto)
    {
        //para validar los datos
        $request->validate([
            'nombre' => 'required',
            'precio' => 'required|numeric|max:1000,00',
        ], [
            //mensajes de validación personalizada
            'required' => 'El :attribute es obligatorio',
            'precio.numeric' => 'El precio del producto debe ser numérico',
            'precio.max' => 'El precio del producto no debe ser mayor a :max',
        ], [
            'nombre' => 'Nombre',
            'precio' => 'Precio',
        ]);

        //actualizar el producto
        $producto->update($request->all());

        //redireccionamos
        return redirect()
            ->route('producto.show', $producto->id)
            ->with('mensaje', 'El producto se ha actualizado correctamente');
    }

    public function show($id)
    {
        $producto = Producto::find($id);

        return view('producto.show', ['producto' => $producto]);
    }


    public function destroy(Producto $producto)
    {
        //eliminamos poroducto
        $producto->delete();

        //redireccionamos
        return redirect()
            ->route('producto.index')
            ->with('mensaje', 'El producto se ha eliminado correctamente');
    }
}
