<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Schema;

class Productotienda extends Model
{
    use HasFactory;

    protected $table = 'productotiendas';

    //campos asignacion masiva
    protected $fillable = [
        'cantidad',
        'producto_id',
        'tienda_id'
    ];

    public static $labels = [
        "cantidad" => "Cantida del producto",
        "producto_id" => "ID del producto",
        "tienda_id" => "ID de la tienda",
        'nombretienda' => 'Nombre de la tienda',
        'nombreproducto' => 'Nombre del producto',      
    ];

    //relaciones con la tabla tienda que es belongsTo y en plural
    public function tienda(): BelongsTo
    {
        return $this->belongsTo(Tienda::class);
    }

    //relaciones con la tabla producto que es belongsTo y en plural
    public function producto(): BelongsTo
    {
        return $this->belongsTo(Producto::class);
    }


    //creo una nueva propiedad en el modelo para ver el nombre de la tienda
    public function getNombretiendaAttribute()
    {
        return $this->tienda->nombre;
    }


    //creo una nueva propiedad en el modelo para ver el nombre del producto
    public function getNombreproductoAttribute()
    {
        return $this->producto->nombre;
    }

    public function getAttributeLabel($attribute)
    {
        return $this->labels[$attribute] ?? $attribute;
    }

    //metodo para obtener los campos de la tabla
    public function getFields()
    {
        return Schema::getColumnListing($this->table);
    }
}
