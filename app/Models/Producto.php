<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Schema;

class Producto extends Model
{
    use HasFactory;

    //nombre de la tabla
    protected $table = 'productos';

    //campos para rellenar de forma masiva
    protected $fillable = [
        'nombre',
        'precio',
    ];

    //creamos una propiedad nueva para las etiquetas 
    public static $labels = [
        "id" => "id del producto",
        "nombre" => "Nombre del producto",
        "precio" => "Precio del producto"
    ];

    //para establecer las relaciones
    //si es un relacion de uno a muchos es hasMany y metodo en plural
    public function productotiendas(): HasMany
    {
        return $this->hasMany(Productotienda::class);
    }

    //metodo para obtener las etiquetas
    public function getAttributeLabel($attribute)
    {
        return self::$labels[$attribute] ?? $attribute;
    }

    //metodo para obtener los campos de la tabla
    public function getFields()
    {
        return Schema::getColumnListing($this->table);
    }
}
