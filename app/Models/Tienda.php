<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Schema;

class Tienda extends Model
{
    use HasFactory;

    //definimos la tabla
    protected $table = 'tiendas';

    //campos para rellenar de forma masiva
    protected $fillable = [
        'nombre',
        'ubicacion',
    ];

    public static $labels = [
        "id" => "id de la tienda",
        "nombre" => "Nombre de la tienda",
        "ubicacion" => "Ubicación de la tienda"
    ];

    //para establecer las relaciones
    //si es un relacion de uno a muchos es hasMany y metodo en plural
    public function productotiendas(): HasMany
    {
        return $this->hasMany(Productotienda::class);
    }

    
    public function getAttributeLabel($attribute){
        return self::$labels[$attribute] ?? $attribute;
    }

    //metodo para obtener los campos de la tabla
    public function getFields()
    {
        return Schema::getColumnListing($this->table);
    }

}
