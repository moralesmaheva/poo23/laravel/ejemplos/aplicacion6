<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Listview extends Component
{
    public $registros;
    public String $modelo;
    public array $campos;
    public bool $comprobar;
    public array $ajenas;

    /**
     * Create a new component instance.
     */
    public function __construct($registros, String $modelo, array $campos=[], array $ajenas = [])
    {
        $this->comprobar = false;
        $this->registros = $registros;
        $this->modelo = $modelo;
        $this->campos = $campos;
        $this->ajenas = $ajenas;
        //comprobamos si los campos llegan vacios
        if(!empty($campos)){
            $this->comprobar = true;
        }
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.listview');
    }
}
