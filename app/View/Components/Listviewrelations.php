<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Listviewrelations extends Component
{

    public $registros;
    public String $modelo;
    public array $campos;


    /**
     * Create a new component instance.
     */
    public function __construct($registros, string $modelo, array $campos = [])
    {
        //inicializamos un array de coleccion de modelos
        //$registros es un array de colecciones
        //$registros[0] es un array de modelos
        //$registros[0]->getAttributes() es un array de atributos
        $this->registros = $registros;
        $this->modelo = $modelo;
        //preguntamos si hay campos estan vacios 
        if (empty($campos)) {
            //aqui te lee el primer registro que es un array con los valores del modelo
            //cojo los keys del primer registro y los meto en campos
            $campos = array_keys($registros[0]->getAttributes());
        }
        //voy a crear un array que le voy a pasar a lla vista y asi solamente
        //la vista tiene que mostrar con una logica
        $this->campos = [];

        //recorro el argumento campos que es el $campos de la vista
        foreach ($campos as $value) {
            //compruebo si el texto de los campos pasados tiene un punto
            //si tiene un punto es una relacion y si no es un campo
            if (str_contains($value, '.')) {
                //extraigo el nombre de la relacion hasta el punto
                $relacion = substr($value, 0, strpos($value, '.'));
                //extraigo el nombre del campo a partir del punto hasta el final
                $campo = substr($value, strpos($value, '.') + 1);
                //añado a la propiedad campos que es un array un nuevo elemento
                // [
                //     'valor',=> utiliza una funcion closure para retornar el valor de la relacion
                //     para llamarla seria $a=['valor]()
                // ]

                //use() se utiliza para meter las variables dentro del ambito actual 
                //que son relacion y campo qyue estan en la vista
                $this->campos[] = ['valor' => function ($modelo) use ($relacion, $campo) {
                    return $modelo->$relacion->$campo;
                }, 'label' => $registros[0]->$relacion->getAttributeLabel($campo)];
            } else {
                //si no tiene un punto es un campo
                //con $registros[0]->getAttributeLabel($value) extraigo el label del campo
                //value es el nombre del campo
                $this->campos[] = ['label' => $registros[0]->getAttributeLabel($value), 'valor' => function ($modelo) use ($value) {
                    return $modelo->$value;
                }];
            }
        }
    }


    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.listviewrelations');
    }
}
