<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('productotiendas', function (Blueprint $table) {
            $table->id();
            $table->integer('cantidad');
            //crear claves ajenas producto
            $table->unsignedBigInteger('producto_id');
            $table
                ->foreign('producto_id') //nombre del campo clave ajena
                ->references('id') //nombre del campo clave principal
                ->on('productos') //nombre de la tabla
                ->onDelete('cascade') //borrar datos en cascada
                ->onUpdate('cascade'); //actualiza datos en cascada

            //crear claves ajenas Tienda
            $table->unsignedBigInteger('tienda_id');
            $table
                ->foreign('tienda_id') //nombre del campo clave ajena
                ->references('id') //nombre del campo clave principal
                ->on('tiendas') //nombre de la tabla
                ->onDelete('cascade') //borrar datos en cascada
                ->onUpdate('cascade'); //actualiza datos en cascada
                
            //indexado sin duplicados
            $table->unique(['producto_id', 'tienda_id']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('productotiendas');
    }
};
