<?php

namespace Database\Seeders;

use App\Models\Producto;
use App\Models\Productotienda;
use App\Models\Tienda;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductotiendaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //hacemos un for para crear mas registros
        for($i=0;$i<10;$i++){    
        //creamos una nueva tienda
        $tienda=Tienda::factory()->create();
        //creamos un nuevo producto
        $producto=Producto::factory()->create();
        //llamamos al seeder de productotienda
        Productotienda::factory()
        //asignamos el tipo de relacion que tiene con tienda
        ->for($tienda)
        //asignamos el tipo de relacion que tiene con producto
        ->for($producto)
        ->create();        
    }
}
}