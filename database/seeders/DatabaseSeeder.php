<?php

namespace Database\Seeders;

use App\Models\User;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        //si no hay seeders
        // Producto::factory(10)->create();
        // Tienda::factory(10)->create();

        //si hay seeders
        $this->call([
            TiendaSeeder::class,
            ProductoSeeder::class,
            ProductotiendaSeeder::class
        ]);
        
        // User::factory(10)->create();

        // User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
    }
}
